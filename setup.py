from distutils.core import setup
# from distutils.extension import Extension 
from setuptools import dist, Extension

# if numpy is not installed, do that first
try: 
	import numpy
except ImportError:
	dist.Distribution().fetch_build_eggs(['numpy>=1.10'])

# if Cython is not available, build C modules from files
try:
    from Cython.Build import cythonize
except ImportError:
    use_cython = False
else:
    use_cython = True

# build cython extensions using Cython from .pyx file and install package
if use_cython:
	extensions=[Extension("cythonized_models.hw", ["cythonized_models/cython_holtwinters.pyx"])]
	setup(name='cythonized_models', author='Tim dHondt',author_email="tim.dhondt@eyeon.nl",
			ext_modules=cythonize(extensions), include_dirs=[numpy.get_include()])

# build cython extensions using Cython from .c file and install package
else:
	
	extensions=[Extension("cythonized_models.hw", ["cythonized_models/cython_holtwinters.c"])]
	setup(name='cythonized_models', author='Tim dHondt',author_email="tim.dhondt@eyeon.nl",
			ext_modules=extensions, include_dirs=[numpy.get_include()])

cimport numpy as np
import numpy as np
from cython cimport cdivision, boundscheck



ctypedef double DTYPE
ctypedef Py_ssize_t index_type

cdef class c_ExponentialSmoothing():
    """
    Holt Winter's Exponential Smoothing, Cythonized

    Parameters
    ----------
    endog : array-like
        Time series
    trend : {"add", "mul", "additive", "multiplicative", None}, optional
        Type of trend component.
    damped : bool, optional
        Should the trend component be damped.
    seasonal : {"add", "mul", "additive", "multiplicative", None}, optional
        Type of seasonal component.
    seasonal_periods : int, optional
        The number of periods in a complete seasonal cycle, e.g., 4 for
        quarterly data or 7 for daily data with a weekly cycle.

    Returns
    -------
    results : ExponentialSmoothing class

    Notes
    -----
    This is a partial implementation of statsmodels's ExponentialSmoothing class in 
    Cython. Note that not all functionalities are implemented.

    Current status:

    - Trend:        done
    - Seasonality:  done
    - Damping:      done
    - Forecasting:  done
    - BoxCox:       TODO
    - Optimization
        using MLE:  TODO
    - Pandas output
        format:     TODO
    - Robust input 
        series:     TODO
    - Floating point
        precision   done

    Contact Tim d'Hondt (tim.dhondt@eyeon.nl | t.dhondt@student.tue.nl | +31657829665)
    for questions, otherwise ask Bram Vercammen (bram.vercammen@eyeon.nl).

    References
    ----------
    .. [1] Hyndman, Rob J., and George Athanasopoulos. Forecasting: principles
        and practice. OTexts, 2014.
    """

    # declaring class attributes with static typing
    # type of trend: mul, add or none
    cdef str trend
    # type of seasonality: mul, add or None
    cdef str seasonal
    # concatenation of the above two for model classification
    cdef str modeltype
    # boolean whether damping is applied
    cdef bint damped
    # boolean whether the model contains trend
    cdef bint trended
    # boolean whether the model contains seasonality
    cdef bint seasoned
    # integer m for seasonality frequency
    cdef int seasonal_periods
    # input series data
    cdef DTYPE[::1] endog
    # number of observations
    cdef index_type nobs

    # For now, keep this python, in the end, evaluate if cythonizing makes any diff
    def __cinit__(self, endog, trend="None", damped=False, seasonal="None",
                 seasonal_periods=None, dates=None, freq=0, missing="None"):

        # set input series
        self.endog = np.asarray(endog)

        # set trend & seasonality type
        self.trend = trend
        self.seasonal = seasonal
        if self.trend == "multiplicative":
            self.trend = "mul"
        elif self.trend == "additive":
            self.trend = "add"
        if self.seasonal == "multiplicative":
            self.seasonal = "mul"
        elif self.seasonal == "additive":
            self.seasonal = "add"

        # set damping boolean
        self.damped = 1 if damped else 0

        # set trended and seasoned booleans
        if not trend == "None":
            self.trended = 1
        else: 
            self.trended = 0
        if not seasonal == "None":
            self.seasoned = 1
        else: 
            self.seasoned = 0

        # Check dimensions of series
        if np.ndim(endog) > 1:
            raise ValueError('endogeneous variables must be 1-dimensional')

        # check if series are strictly positive in case of Mul Mul model
        if (self.trend == 'mul' or self.seasonal == 'mul') and \
                np.any(np.asarray(endog) <= 0.0):
            raise ValueError('endog must be strictly positive when using'
                             'multiplicative trend or seasonal components.')

        # check for damping but not trending
        if self.damped == 1 and self.trend == "None":
            raise ValueError('Can only dampen the trend component')
        
        # Set seasonal periods
        if not seasonal_periods == None:
            self.seasonal_periods = seasonal_periods
            # convert a pandas frequency to a periodicity
            # if seasonal_periods is None:
            #     self.seasonal_periods = freq_to_period(self._index_freq)
            # check length of seasonality
            if self.seasonal_periods <= 1:
                raise ValueError('seasonal_periods must be larger than 1.')
        # if no seasoning, set periods to zero
        else:
            self.seasonal_periods = 0

        if self.seasoned==1 and self.seasonal_periods==None:
            raise NotImplementedError('seasonal_periods must be set when using seasonality, as estimation is not yet implemented')
        # number of observations
        self.nobs = len(self.endog)
        # model type: concatenation of trend and seasonality type
        self.modeltype = trend+seasonal

    
        
    def fit(self, smoothing_level=0.0, smoothing_slope=0.0, smoothing_seasonal=0.0,
        damping_slope=0.0, optimized=False, use_boxcox=False, remove_bias=False,
        use_basinhopping=False, start_params=None, initial_level=None, initial_slope=None,
        use_brute=False):
        """
        Fit the model

        Parameters
        ----------
        smoothing_level : float, optional
            The alpha value of the simple exponential smoothing, if the value
            is set then this value will be used as the value.
        smoothing_slope :  float, optional
            The beta value of the Holt's trend method, if the value is
            set then this value will be used as the value.
        smoothing_seasonal : float, optional
            The gamma value of the holt winters seasonal method, if the value
            is set then this value will be used as the value.
        damping_slope : float, optional
            The phi value of the damped method, if the value is
            set then this value will be used as the value.
        optimized : bool, optional
            Estimate model parameters by maximizing the log-likelihood
        use_boxcox : {True, False, 'log', float}, optional
            Should the Box-Cox transform be applied to the data first? If 'log'
            then apply the log. If float then use lambda equal to float.
        remove_bias : bool, optional
            Remove bias from forecast values and fitted values by enforcing
            that the average residual is equal to zero.
        use_basinhopping : bool, optional
            Using Basin Hopping optimizer to find optimal values
        start_params: array, optional
            Starting values to used when optimizing the fit.  If not provided,
            starting values are determined using a combination of grid search
            and reasonable values based on the initial values of the data
        initial_level: float, optional
            Value to use when initializing the fitted level.
        initial_slope: float, optional
            Value to use when initializing the fitted slope.
        use_brute: bool, optional
            Search for good starting values using a brute force (grid)
            optimizer. If False, a naive set of starting values is used.

        Returns
        -------
        results : HoltWintersResults class
            See statsmodels.tsa.holtwinters.HoltWintersResults

        Notes
        -----
        This implementation lacks optimization component. Which was its main 
        purpose in the statsmodels package. To be implemented in the future right
        here

        References
        ----------
        [1] Hyndman, Rob J., and George Athanasopoulos. Forecasting: principles
            and practice. OTexts, 2014.
        """
        if optimized:
            raise NotImplementedError("Optimization has not been implemented yet")
        if use_boxcox:
            raise NotImplementedError("Box-Cox has not been implemented yet")
        if use_brute:
            raise NotImplementedError("Grid optimization has not been implemented yet")
        if remove_bias:
            raise NotImplementedError("Remove bias has not been implemented yet")
        if use_basinhopping:
            raise NotImplementedError("Basinhopping has not been implemented yet")
        if not start_params == None:
            raise NotImplementedError("Starting parameters has not been implemented yet")
        
        # Declare variables into native C types for speed
        cdef DTYPE alpha = smoothing_level
        cdef DTYPE beta = smoothing_slope
        cdef DTYPE gamma = smoothing_seasonal
        cdef DTYPE phi = damping_slope if self.damped == 1 else 1.0
        cdef DTYPE l0, b0
        cdef DTYPE[:] s0
        cdef DTYPE[:] fitted
        # initialize typed memoryviews at point 0
        (l0, b0, s0) = self.initial_values(self.endog, self.trend, self.seasonal, self.seasonal_periods, self.seasoned, self.trended, self.nobs)

        '''
        _______________________________________________________________________________
        NOTE:
        optimization here was removed for now, but can be implemented in the future by cythonizing the statsmodels library further (TODO)
        '''

        fitted = self._predict(h=0, smoothing_level=alpha, smoothing_slope=beta,
                              smoothing_seasonal=gamma, damping_slope=phi,
                              initial_level=l0, initial_slope=b0, initial_seasons=s0,
                              use_boxcox=0, remove_bias=0)
        


        params = {'smoothing_level': alpha,
                      'smoothing_slope': beta,
                      'smoothing_seasonal': gamma,
                      'damping_slope': phi if self.damped == 1 else np.nan,
                      'initial_level': l0,
                      'initial_slope': b0 / phi,
                      'initial_seasons': s0}
        # hwfit._results.mle_retvals = opt
        return Results(params, fitted, fitted[: - 1], fitted[- 1:])
    
    @cdivision
    @boundscheck(False)
    cdef DTYPE[:] _predict(self, DTYPE[:] initial_seasons, int h=0, DTYPE smoothing_level=0, DTYPE smoothing_slope=0,
             DTYPE smoothing_seasonal=0, DTYPE initial_level=0, DTYPE initial_slope=0,
             DTYPE damping_slope=0, bint use_boxcox=0, 
             bint remove_bias=0):
        """
        Helper prediction function

        Parameters
        ----------
        h : int, optional
            The number of time steps to forecast ahead.
        """
        # Variable renames to alpha, beta, etc as this helps with following the
        # mathematical notation in general
        # Declare variables into native C types for speed
        cdef DTYPE alpha = smoothing_level
        cdef DTYPE beta = smoothing_slope
        cdef DTYPE gamma = smoothing_seasonal
        cdef bint damped = self.damped
        cdef DTYPE phi = damping_slope if damped==1 else 1.0
        # ((1-par) versions of these parameters)
        cdef DTYPE alphac = 1.0 - alpha
        cdef DTYPE betac = 1.0 - beta
        cdef DTYPE gammac = 1.0 - gamma
        cdef int m = self.seasonal_periods
        cdef str trend = self.trend
        cdef str seasonal = self.seasonal
        cdef index_type nobs = self.nobs
        cdef bint trended = self.trended
        cdef bint seasoned = self.seasoned
        # Turn arrays into typed memoryviews for speed
        cdef DTYPE [:] y = self.endog
        cdef DTYPE [:] l = np.zeros((nobs + h + 1,), dtype=np.float64)
        cdef DTYPE [:] b = np.zeros((nobs + h + 1,), dtype=np.float64)
        cdef DTYPE [:] s = np.zeros((nobs + h + m + 1,), dtype=np.float64)
        cdef DTYPE [:] fitted = np.zeros((nobs + h + 1,), dtype=np.float64)
        # initialize typed memoryviews at point 0
        l[0] = initial_level
        b[0] = initial_slope
        s[:m] = initial_seasons

        cdef DTYPE[:] phi_h = np.cumsum(np.repeat(phi, h + 1)**np.arange(1, h + 1 + 1), dtype=np.float64) if damped == 1 else np.arange(1, h + 1 + 1, dtype=np.float64)
        
        # NOTE: Box-Cox here was removed but can be re-implemented (TODO)

        '''
        We are going to use this string to categorize the model we will be using.
        Original implementation was very pythonic, this way is more suited for cython
        and easier to understand for future improvements.

        Theory was taken from [1] at https://otexts.com/fpp2/holt-winters.html

        Possibilities ("TrendSeasonal"):
        ------------------------------------------
        - NoneNone
        - NoneAdd
        - NoneMul
        - AddNone
        - MulNone
        - AddAdd
        - MulAdd
        - AddMul
        - MulMul
        '''
        cdef str modeltype = self.modeltype
        cdef index_type t, k
        # Single Exponential Smoothing
        if modeltype == 'NoneNone':
            fitted[0] = l[0]
            for t in range(1, nobs + 1):
                l[t] = alpha * y[t - 1] + alphac * l[t - 1]
                fitted[t] = l[t]
            fitted[nobs:] = l[nobs] 

        # No trend, Additive seasonality
        elif modeltype == 'Nonemul':
            fitted[0] = l[0] * s[m - 1]
            for t in range(1, nobs + 1):
                l[t] = alpha * (y[t] / s[t - 1]) + alphac * l[t - 1]
                s[t + m - 1] = gamma * (y[t] / l[t - 1]) + gammac * s[t - 1]
                fitted[t] = l[t] * s[t + m - 1]
            for k in range(h + 1):
                fitted[nobs + k] = l[nobs] * s[nobs - 1 + k]
        
        # No trend, Multiplicative seasonality
        elif modeltype == 'Noneadd':
            fitted[0] = l[0] + s[m - 1]
            for t in range(1, nobs + 1):
                l[t] = alpha * (y[t - 1] - s[t - 1]) + alphac * l[t - 1]
                s[t + m - 1] = gamma * (y[t] - l[t - 1]) + gammac * s[t - 1]
                fitted[t] = l[t] + s[t + m - 1]
            for k in range(h + 1):
                fitted[nobs + k] = l[nobs] + s[nobs - 1 + k]

        # Additive trend, no seasonality
        elif modeltype == 'addNone':
            fitted[0] = l[0] + b[0]
            for t in range(1, nobs + 1):
                l[t] = alpha * y[t - 1] + alphac * (l[t - 1] + b[t - 1] * phi)
                b[t] = beta * (l[t] - l[t - 1]) + betac * b[t - 1] * phi
                fitted[t] = l[t] + b[t]
            for k in range(h + 1):
                fitted[nobs + k] = l[nobs] + b[nobs]*phi_h[k]

        # Multiplicative trend, no seasonality
        elif modeltype == 'mulNone':
            fitted[0] = l[0] * (b[0] ** phi_h[0])
            for t in range(1, nobs + 1):
                l[t] = alpha * y[t - 1] + alphac * (l[t - 1] * (b[t - 1] ** phi))
                b[t] = beta * (l[t] / l[t - 1]) + (betac * (b[t - 1] ** phi))
                fitted[t] = l[t] * (b[t] ** phi_h[0])
            for k in range(h + 1):
                fitted[nobs + k] = l[nobs] * (b[nobs] ** phi_h[k])

        # Additive trend, additive seasonality
        elif modeltype == 'addadd':
            fitted[0] = l[0] + (b[0] * phi_h[0]) + s[0]
            for t in range(1, nobs + 1):
                l[t] = alpha * (y[t - 1] - s[t - 1]) + alphac * (l[t - 1] + b[t - 1] * phi)
                b[t] = beta * (l[t] - l[t - 1]) + betac * b[t - 1] * phi
                s[t + m - 1] = gamma * (y[t - 1] - l[t - 1] - b[t - 1] * phi) + gammac * s[t - 1]
                fitted[t] = l[t] + b[t] + s[t]
            for k in range(h + 1):
                fitted[nobs + k] = l[nobs] + (b[nobs] * phi_h[k]) + s[nobs - 1 + k]

        # Multiplicative trend, additive seasonality
        elif modeltype == 'muladd':
            fitted[0] = l[0] * (b[0] ** phi_h[0]) + s[0]
            for t in range(1, nobs + 1):
                l[t] = alpha * (y[t - 1] - s[t - 1]) + alphac * (l[t - 1] * (b[t - 1] ** phi))
                b[t] = beta * (l[t] / l[t - 1]) + betac * (b[t - 1] ** phi)
                s[t + m - 1] = gamma * (y[t - 1] - (l[t - 1] * (b[t - 1]) ** phi)) + gammac * s[t - 1]
                fitted[t] = l[t] * (b[t] ** phi_h[0]) + s[t]
            for k in range(h + 1):
                fitted[nobs + k] = l[nobs] * (b[nobs] ** phi_h[k]) + s[nobs - 1 + k]

        # Additive trend, multiplicative seasonality
        elif modeltype == 'addmul':
            fitted[0] = (l[0] + b[0] * phi_h[0])*s[0] 
            for t in range(1, nobs + 1):
                l[t] = alpha * (y[t - 1] / s[t - 1]) + alphac * (l[t - 1] + b[t - 1] * phi)
                b[t] = beta * (l[t] - l[t - 1]) + betac * b[t - 1] * phi
                s[t + m - 1] = gamma * (y[t - 1] / (l[t - 1] + b[t - 1] * phi)) + gammac * s[t - 1]
                fitted[t] = (l[t] + b[t] * phi_h[0]) * s[t]
            for k in range(h + 1):
                fitted[nobs + k] = (l[nobs] + (b[nobs] * phi_h[k])) * s[nobs - 1 + k]

        # Multiplicative trend, multiplicative seasonality
        elif modeltype == 'mulmul':
            fitted[0] = l[0] * (b[0] ** phi_h[0]) * s[0]
            for t in range(1, nobs + 1):
                l[t] = alpha * (y[t - 1] / s[t - 1]) + alphac * (l[t - 1] * (b[t - 1] ** phi))
                b[t] = beta * (l[t] / l[t - 1]) + betac * (b[t - 1] ** phi)
                s[t + m - 1] = gamma * (y[t - 1] / (l[t - 1]*(b[t - 1] ** phi))) + gammac * s[t - 1]
                fitted[t] = l[t] * (b[t] ** phi_h[0]) * s[t]
            for k in range(h + 1):
                fitted[nobs + k] = (l[nobs] * (b[nobs] ** phi_h[k])) * s[nobs - 1 + k]

        return fitted


    @cdivision
    @boundscheck(False)
    def initial_values(self, DTYPE[:] y, str trend, str seasonal, int m, bint seasoning, bint trending, index_type nobs):
        """
        Compute initial values used in the exponential smoothing recursions

        Returns
        -------
        initial_level : float
            The initial value used for the level component
        initial_slope : {float, None}
            The initial value used for the trend component
        initial_seasons : list
            The initial values used for the seasonal components

        Notes
        -----
        Convenience function the exposes the values used to initialize the
        recursions. When optimizing parameters these are used as starting
        values.

        Method used can be found here:
        https://robjhyndman.com/hyndsight/hw-initialization/
        """

        cdef DTYPE total = 0.0
        cdef DTYPE[:] s0 = np.zeros(m, dtype=np.float64)
        cdef index_type i
        cdef DTYPE b0 = 0.0
        cdef DTYPE l0 = 0.0
        cdef DTYPE[:] lead = y[m:m + m]           
        cdef DTYPE[:] lag = y[:m]
        cdef DTYPE total1 = 0.0
        cdef DTYPE total2 = 0.
        cdef DTYPE total3 = 0.0
        cdef DTYPE meanlead, meanlag, meanboth
        if seasoning == 1:
            # mean of all indexes that are factor of m
            for i in range(0, nobs, m):
                total += y[i]
            l0 = total / len(range(0, nobs, m))
            
            # calculate lead & lag mean
            
            for i in range(0, m):
                total1 += lead[i]
                total2 += lag[i]
                total3 += (lead[i]-lag[i])/ m
            meanlead = total1 / m
            meanlag = total2 / m
            meanboth = total3 / m

            if trend == 'mul':
                b0 = (meanlead - meanlag) / m
            elif trend == 'add':
                b0 = meanboth
            # first seasonality factors are
            if seasonal == 'mul':
                for i in range(0 , m):
                    s0[i] = y[i] / l0
            else: 
                for i in range(0 , m):
                    s0[i] = y[i] - l0

        elif trending == 1:
            l0 = y[0]
            if trend == 'mul':
                b0 = y[1] / y[0]
            else:
                b0 = y[1] - y[0]
        else:
            l0 = y[0]

        return l0, b0, s0

    def forecast(self, smoothing_level=0.0, smoothing_slope=0.0, smoothing_seasonal=0.0,
        damping_slope=0.0, optimized=False, use_boxcox=False, remove_bias=False,
        use_basinhopping=False, start_params=None, initial_level=None, initial_slope=None,
        use_brute=False, h=1):
        """
        Fit & forecast

        Parameters
        ----------
        smoothing_level : float, optional
            The alpha value of the simple exponential smoothing, if the value
            is set then this value will be used as the value.
        smoothing_slope :  float, optional
            The beta value of the Holt's trend method, if the value is
            set then this value will be used as the value.
        smoothing_seasonal : float, optional
            The gamma value of the holt winters seasonal method, if the value
            is set then this value will be used as the value.
        damping_slope : float, optional
            The phi value of the damped method, if the value is
            set then this value will be used as the value.
        optimized : bool, optional
            Estimate model parameters by maximizing the log-likelihood
        use_boxcox : {True, False, 'log', float}, optional
            Should the Box-Cox transform be applied to the data first? If 'log'
            then apply the log. If float then use lambda equal to float.
        remove_bias : bool, optional
            Remove bias from forecast values and fitted values by enforcing
            that the average residual is equal to zero.
        use_basinhopping : bool, optional
            Using Basin Hopping optimizer to find optimal values
        start_params: array, optional
            Starting values to used when optimizing the fit.  If not provided,
            starting values are determined using a combination of grid search
            and reasonable values based on the initial values of the data
        initial_level: float, optional
            Value to use when initializing the fitted level.
        initial_slope: float, optional
            Value to use when initializing the fitted slope.
        use_brute: bool, optional
            Search for good starting values using a brute force (grid)
            optimizer. If False, a naive set of starting values is used.
        h: int, optional
            How many timesteps ahead one would like to forecast

        Returns
        -------
        forecasted values

        Notes
        -----
        This function directly returns forecasted values, instead of using .fit().forecast(h),
        which would fit the model 2 times actually,

        References
        ----------
        [1] Hyndman, Rob J., and George Athanasopoulos. Forecasting: principles
            and practice. OTexts, 2014.
        """
        if optimized:
            raise NotImplementedError("Optimization has not been implemented yet")
        if use_boxcox:
            raise NotImplementedError("Box-Cox has not been implemented yet")
        if use_brute:
            raise NotImplementedError("Grid optimization has not been implemented yet")
        if remove_bias:
            raise NotImplementedError("Remove bias has not been implemented yet")
        if use_basinhopping:
            raise NotImplementedError("Basinhopping has not been implemented yet")
        if not start_params == None:
            raise NotImplementedError("Starting parameters has not been implemented yet")
        
        # Declare variables into native C types for speed
        cdef DTYPE alpha = smoothing_level
        cdef DTYPE beta = smoothing_slope
        cdef DTYPE gamma = smoothing_seasonal
        cdef DTYPE phi = damping_slope if self.damped == 1 else 1.0
        cdef DTYPE l0, b0
        cdef DTYPE[:] s0
        cdef DTYPE[:] fitted
        # initialize typed memoryviews at point 0
        (l0, b0, s0) = self.initial_values(self.endog, self.trend, self.seasonal, self.seasonal_periods, self.seasoned, self.trended, self.nobs)

        '''
        _______________________________________________________________________________
        NOTE:
        optimization here was removed for now, but can be implemented in the future by cythonizing the statsmodels library further (TODO)
        '''

        fitted = self._predict(h=h, smoothing_level=alpha, smoothing_slope=beta,
                              smoothing_seasonal=gamma, damping_slope=phi,
                              initial_level=l0, initial_slope=b0, initial_seasons=s0,
                              use_boxcox=0, remove_bias=0)
        
        return np.asarray(fitted[-h:])



#Class which is returned after fitting the model!
class Results():
     
    def __init__(self, params, fittedfcast, fittedvalues, fcastvalues):
        self.params = params
        self.fittedfcast = np.asarray(fittedfcast)
        self.fittedvalues = np.asarray(fittedvalues)
        self.fcastvalues = np.asarray(fcastvalues)


